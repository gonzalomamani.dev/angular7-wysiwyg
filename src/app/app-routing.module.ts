import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SimpleExampleComponent} from './components/simple-example/simple-example.component';

const routes: Routes = [
  {path: 'simple-example', component: SimpleExampleComponent},
  {path: '', component: SimpleExampleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
