import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SimpleExampleComponent } from './components/simple-example/simple-example.component';
// import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { CKEditorModule } from './components/shared/ckeditor/ckeditor.module';
import { ModalObservacionComponent } from './components/simple-example/components/modal-observacion/modal-observacion.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    SimpleExampleComponent,
    ModalObservacionComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    CKEditorModule,
    AppRoutingModule,
    NgbModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
