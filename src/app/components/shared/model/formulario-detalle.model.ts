export class FormularioDetalle {
  public id: number;
  public detalle: string;
  public observacion: string;


  constructor(id: number, detalle: string, observacion: string) {
    this.id = id;
    this.detalle = detalle;
    this.observacion = observacion;
  }
}
