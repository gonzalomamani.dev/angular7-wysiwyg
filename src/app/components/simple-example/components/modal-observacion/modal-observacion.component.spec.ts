import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalObservacionComponent } from './modal-observacion.component';

describe('ModalObservacionComponent', () => {
  let component: ModalObservacionComponent;
  let fixture: ComponentFixture<ModalObservacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalObservacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalObservacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
