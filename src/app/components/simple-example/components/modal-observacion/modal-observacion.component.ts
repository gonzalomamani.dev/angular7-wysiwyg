import {Component, Input, OnInit} from '@angular/core';
import {FormularioDetalle} from '../../../shared/model/formulario-detalle.model';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-modal-observacion',
  templateUrl: './modal-observacion.component.html',
  styleUrls: ['./modal-observacion.component.sass']
})
export class ModalObservacionComponent implements OnInit {
  @Input() attribsModal: any;
  @Input() modalReference: NgbModalRef;
  public Editor = ClassicEditor;
  public config = {
    toolbar: ['bold', 'italic', '|', 'bulletedList', '|', 'undo', 'redo' ],
    placeholder: 'Ingrese una observación'
  };
  constructor() { }

  ngOnInit() {
  }
  cerrarModal() {
    this.modalReference.close();
  }
}
