import {Component, OnInit, ViewChild} from '@angular/core';
// import { CKEditor5, BlurEvent, ChangeEvent, FocusEvent } from '../shared/ckeditor';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {CKEditor5} from '@ckeditor/ckeditor5-angular/ckeditor';
import Editor = CKEditor5.Editor;
import {FormularioDetalle} from '../shared/model/formulario-detalle.model';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
// import * as ClassicEditor from '../shared/ckeditor';
@Component({
  selector: 'app-simple-example',
  templateUrl: './simple-example.component.html',
  styleUrls: ['./simple-example.component.sass']
})
export class SimpleExampleComponent implements OnInit {
  @ViewChild('modalObservacion') modalDetalleHistorial;
  modalReference: NgbModalRef;
  public Editor = ClassicEditor;
  public isDisabled = false;
  public formularios: FormularioDetalle[];
  public config: any;
  public observacion: string;
  public attribsModal: any;
  constructor(
    private _modalService: NgbModal
  ) {
    this.formularios = [];
    this.config = {
      toolbar: ['bold', 'italic', '|', 'bulletedList', '|', 'undo', 'redo' ],
      placeholder: 'Ingrese una observación'
    };
    this.observacion = null;
  }

  ngOnInit() {
    this.formularios.push(new FormularioDetalle(1, 'Información del Interesado o Solicitante', null));
    this.formularios.push(new FormularioDetalle(2, 'Nacionalidad y domicilio', null));
    this.formularios.push(new FormularioDetalle(3, 'Verificar Inventores (nombre, firmas y rubricas)', null));
    this.formularios.push(new FormularioDetalle(4, 'Verificar si los datos 2 y 3 coinciden con la cesión', null));
    this.formularios.push(new FormularioDetalle(5, 'Copia traducida (fotocopia simple)', null));
  }
  openModal(event, formulario: FormularioDetalle): void {
    if (event !== null) { event.stopPropagation(); }
    const index: number = this.formularios.indexOf(formulario);
    if (index !== -1) {
      this.formularios[index]
      this.attribsModal = {
        'titleModal': 'Historial',
        'formulario' : this.formularios[index]};
      const content = this.modalDetalleHistorial;
      this.modalReference = this._modalService.open(content, {ariaLabelledBy: 'modal-basic-title', keyboard: true, backdrop: true});
    }
  }
}
